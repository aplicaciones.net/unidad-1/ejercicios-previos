﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practica_1
{
    class Practica_1
    {
        Int16 a, b, c;
        Int16 res;


        void operar(Int16 primero, Int16 segundo, Int16 tercero)
        {
            res = Convert.ToInt16(primero + segundo + tercero);
        }

        void imprimir()
        {
            Console.Write("La suma de los valores es: ");
            Console.WriteLine(res);
            Console.ReadLine();

        }

        static void Main(string[] args)
        {
            Practica_1 obj = new Practica_1();

            Console.WriteLine("Dame el primer valor:");
            obj.a = Convert.ToInt16( Console.ReadLine());

            Console.WriteLine("Dame el segundo valor:");
            obj.b = Convert.ToInt16(Console.ReadLine());

            Console.WriteLine("Dame el tercer valor:");
            obj.c = Convert.ToInt16(Console.ReadLine());

            obj.operar(obj.a, obj.b, obj.c);
            obj.imprimir();
        }
    }
}
