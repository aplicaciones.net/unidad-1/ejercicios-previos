﻿namespace Practica_2
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.valor_1 = new System.Windows.Forms.TextBox();
            this.valor_3 = new System.Windows.Forms.TextBox();
            this.valor_2 = new System.Windows.Forms.TextBox();
            this.resultado = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.veces = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // valor_1
            // 
            this.valor_1.Location = new System.Drawing.Point(38, 54);
            this.valor_1.Name = "valor_1";
            this.valor_1.Size = new System.Drawing.Size(100, 20);
            this.valor_1.TabIndex = 0;
            this.valor_1.TextChanged += new System.EventHandler(this.valor_1_TextChanged);
            // 
            // valor_3
            // 
            this.valor_3.Location = new System.Drawing.Point(38, 106);
            this.valor_3.Name = "valor_3";
            this.valor_3.Size = new System.Drawing.Size(100, 20);
            this.valor_3.TabIndex = 1;
            this.valor_3.TextChanged += new System.EventHandler(this.valor_3_TextChanged);
            // 
            // valor_2
            // 
            this.valor_2.Location = new System.Drawing.Point(38, 80);
            this.valor_2.Name = "valor_2";
            this.valor_2.Size = new System.Drawing.Size(100, 20);
            this.valor_2.TabIndex = 2;
            this.valor_2.TextChanged += new System.EventHandler(this.valor_2_TextChanged);
            // 
            // resultado
            // 
            this.resultado.Enabled = false;
            this.resultado.Location = new System.Drawing.Point(176, 54);
            this.resultado.Name = "resultado";
            this.resultado.Size = new System.Drawing.Size(100, 20);
            this.resultado.TabIndex = 3;
            this.resultado.TextChanged += new System.EventHandler(this.resultado_TextChanged);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(176, 106);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(100, 23);
            this.button1.TabIndex = 4;
            this.button1.Text = "Calcular";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // veces
            // 
            this.veces.Enabled = false;
            this.veces.Location = new System.Drawing.Point(95, 12);
            this.veces.Name = "veces";
            this.veces.Size = new System.Drawing.Size(100, 20);
            this.veces.TabIndex = 5;
            this.veces.TextChanged += new System.EventHandler(this.veces_TextChanged);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(305, 183);
            this.Controls.Add(this.veces);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.resultado);
            this.Controls.Add(this.valor_2);
            this.Controls.Add(this.valor_3);
            this.Controls.Add(this.valor_1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox valor_1;
        private System.Windows.Forms.TextBox valor_3;
        private System.Windows.Forms.TextBox valor_2;
        private System.Windows.Forms.TextBox resultado;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox veces;
    }
}

